# wec
webextension communication

### Installation
```sh
$ yarn add gitlab:dbdii407/wec
```

### Script Example
background.js
```js
"use strict"

const wec = require('wec')

let bg = wec.Background({
  protocols: ['file', 'http']
})

bg.on('tab', tab => {
  tab.on('close', () => console.log('Uh oh! You closed a tab.'))
  
  // This will not work. Can only be used after page is loaded.
  tab.send('test').then(resp => console.log(resp))

  tab.on('complete', () => {
    // This will work!
    tab.send('test').then(resp => console.log(resp))
  })
    
  // The content script wants our lucky number!
  tab.with('number', () => new Promise(res => {
    setTimeout(() => res(5), 1500)
  }))
})
```

script.js
```js
"use strict"

const wec = require('wec')

let br = wec.Page()

// Background script asking for something!
br.with('test', () => new Promise(res => {
  setTimeout(() => res('Ohai!'), 2000)
}))

// Ask for the lucky number!
br.send('number').then(resp => console.log(`The lucky number is ${resp}!`))
```