"use strict"

const alias = require('@bit/ptyps.t.function.alias')
const next = require('@bit/ptyps.t.promise.next')
const debug = require('debug')('wec:Layer')
const pathregex = require('path-to-regex')

module.exports = function Layer() {
  if (!(this instanceof Layer))
    return new Layer()

  let stack = []

  this.get = hash => {
    debug('get', hash)

    let req = {}

    return stack.reduce((list, {func, info}) => {
      let result = info.regexp.exec(hash)
      if (!result) return list

      list.push(done => func.emit('run', req, done))
      return list
    }, [])
  }

  this.use = (hash, func) => {
    if (hash && !func)
      func = hash, hash = null

    debug('use', hash)

    stack.push({
      info: hash ? new pathregex(hash) : {
        regexp: /(.*)/, keys: []
      },

      func: func,
    })
  }
  
  this.dispatch = hash => new Promise(res => {
    debug('dispatch', hash)

    let list = this.get(hash)
    !list.length ? res() : next(list).then(res)
  })

  this.run = alias(this.dispatch)
}
