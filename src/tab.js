"use strict"

const inherits = require('@bit/ptyps.t.function.inherits')
const has = require('@bit/ptyps.t.array.has')
const uri = require('@bit/ptyps.t.utils.url')
const web = require('webextension-polyfill')
const debug = require('debug')('wec:Tab')
const q = require('bluebird')

module.exports = function Tab({id: identifier}) {
  if (!(this instanceof Tab))
    return new Tab({id: identifier})

  let list = {}, last = {
    status: '',
    hash: ''
  }

  let _ = inherits(this, 'EventEmitter', {
    emit: (event, ...args) => {
      debug(`emit`, event)
      this.__proto__.emit.apply(this, [].concat(event, args))
    },

    info: () =>  new q.try(() => web.tabs.get(identifier)).then(tab => {
      let url = uri.parse(tab.url)

      return Object.assign(url, {
        id: identifier
      })
    }),

    run: (file, run='document_start') => new q.try(() => debug(`run`, file, run))
      .then(() => web.tabs.executeScript(identifier, {
        file: file,
        runAt: run
      })),

    refresh: () => new q.try(() => debug(`refresh`))
      .then(() => web.tabs.reload(identifier)),

    focus: () => new q.try(() => debug(`focus`))
      .then(() => web.tabs.update(identifier, {active: true})),

    with: (event, fn) => new q.try(() => {
      if (has(list, event))
        return

      debug(`with`, event)
      list[event] = fn
    }),

    send: (event, ...args) => new q.try(() => debug(`send`, event))
      .then(() => web.tabs.sendMessage(identifier, {
        event: {
          name: event,
          args: args
        }
      })),

    dispatch: event => new q.try(() => {
      if (!has(list, event.name))
        return

      debug(`dispatch`, event.name)
      return list[event.name].apply(null, event.args)
    })
  })

  web.tabs.onRemoved.addListener(function onRemoved(id) {
    if (identifier != id)
      return

    web.tabs.onRemoved.removeListener(onRemoved)
    _.emit('close')
  })

  function onUpdated({}, {}, details) {
    if (details.id != identifier || !details.url)
      return

    let url = uri.parse(details.url)

    if (last.hash == url.hash || last.status != 'complete')
      return

    last.hash = url.hash

    _.emit('hash', url.hash)
  }

  web.tabs.onUpdated.addListener(onUpdated)

  function onBeforeNavigate(details) {
    if (details.tabId != identifier || last.status == 'refresh')
      return

    last.status = 'refresh'
    last.hash = ''
    // list = {}

    _.emit('refresh')
  }

  web.webNavigation.onBeforeNavigate.addListener(onBeforeNavigate)

  function onDOMContentLoaded(details) {
    if (details.tabId != identifier || last.status == 'loading')
      return

    last.status = 'loading'

    _.emit('loading')
  }

  web.webNavigation.onDOMContentLoaded.addListener(onDOMContentLoaded)

  function onCompleted(details) {
    if (details.tabId != identifier || last.status == 'completed')
      return

    last.status = 'complete'

    _.emit('complete')
  }

  web.webNavigation.onCompleted.addListener(onCompleted)

  function onMessage({event}, {tab: {id}}) {
    if (id != identifier || !event)
      return

    _.dispatch(event)
  }

  web.runtime.onMessage.addListener(onMessage)

  this.once('close', () => {
    web.webNavigation.onBeforeNavigate.removeListener(onBeforeNavigate)
    web.webNavigation.onDOMContentLoaded.removeListener(onDOMContentLoaded)
    web.webNavigation.onCompleted.removeListener(onCompleted)

    web.tabs.onUpdated.removeListener(onUpdated)
    
    web.runtime.onMessage.removeListener(onMessage)
  })
}