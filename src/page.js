"use strict"

const inherits = require('@bit/ptyps.t.function.inherits')
const has = require('@bit/ptyps.t.array.has')
const web = require('webextension-polyfill')
const debug = require('debug')('wec:Page')
const q = require('bluebird')

module.exports = function Page() {
  if (!(this instanceof Page))
    return new Page()

  let list = {}

  let _ = inherits(this, 'EventEmitter', {
    emit: (event, ...args) => {
      debug(`emit`, event)
      this.__proto__.emit.apply(this, [].concat(event, args))
    },

    with: (event, fn) => new q.try(() => {
      if (has(list, event))
        return

      debug(`with`, event)
      list[event] = fn
    }),

    send: (event, ...args) => new q.try(() => debug(`send`, event))
      .then(() => web.runtime.sendMessage({
        event: {
          name: event,
          args: args
        }
      })),
  })

  function onMessage({event}) {
    if (event) return new q.try(() => {
      if (!has(list, event.name))
        return

      let fn = list[event.name]
      return fn.apply(null, event.args)
    })
  }

  web.runtime.onMessage.addListener(onMessage)

  window.addEventListener('beforeunload', () => this.send('reset'))

  document.addEventListener('readystatechange', () => {
    if (document.readyState == 'interactive')
      return this.emit(':loading')

    if (document.readyState == 'complete')
      return this.emit(':complete')
  })
}