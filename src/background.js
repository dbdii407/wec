"use strict";

const inherits = require('@bit/ptyps.t.function.inherits')
const debug = require('debug')('wec:Background')
const uri = require('@bit/ptyps.t.utils.url')
const web = require('webextension-polyfill')
const q = require('bluebird')
const tab = require('./tab')

module.exports = function Background() {
  if (!(this instanceof Background))
    return new Background()

  let list = [], createTab = details => {
    let t = tab(details)

    t.once('close', () => {
      let i = list.indexOf(t)
      list.splice(i, 1)
    })

    // Will throw an error be if we don't return a Promise, due to using .each()
    return new q.try(() => this.emit('tab', t))
  }

  let _ = inherits(this, 'EventEmitter', {
    emit: (event, ...args) => {
      debug('emit', event)
      this.__proto__.emit.apply(this, [].concat(event, args))
    },

    tabs: () => new q.try(() => debug('tabs')).then(() => web.tabs.query({})).map(tab => {
      let result = uri.parse(tab.url)

      return Object.assign(result, {
        id: tab.id
      })
    }),

    refresh: id => new q.try(() => debug('refresh'))
      .then(() => web.tabs.reload(id)),

    focus: id => new q.try(() => debug('focus'))
      .then(() => web.tabs.update(id, {active: true})),
    
    open: url => new q.try(() => debug('open'))
      .then(() => web.tabs.create({url: url}))
  })

  web.runtime.onInstalled.addListener(() => new q.try(() => this.emit('install'))
    .then(() => web.tabs.query({})).each(createTab)
    .then(() => this.emit('installed'))
  )

  web.tabs.onCreated.addListener(details => new q.try(() => this.emit('create'))
    .then(() => createTab(details)))
}