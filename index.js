"use strict"

module.exports = {
  Background: require('./src/background'),
  Layer: require('./src/layer'),
  Page: require('./src/page')
}